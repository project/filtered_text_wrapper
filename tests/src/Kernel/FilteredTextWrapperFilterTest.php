<?php

declare(strict_types=1);

namespace Drupal\Tests\filtered_text_wrapper\Kernel;

use Drupal\Core\Language\Language;
use Drupal\filter\FilterPluginCollection;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test the filtered text wrapper filter.
 *
 * @group filtered_text_wrapper
 */
class FilteredTextWrapperFilterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'filter',
    'filtered_text_wrapper',
  ];

  /**
   * The wrapper filter.
   *
   * @var \Drupal\filter\Plugin\FilterInterface
   */
  protected $wrapperFilter;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['system']);

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $filterPluginManager */
    $filterPluginManager = $this->container->get('plugin.manager.filter');
    $this->wrapperFilter = (new FilterPluginCollection($filterPluginManager, []))
      ->get('wrapper');
  }

  /**
   * Test the wrapper filter.
   *
   * @dataProvider provideWrapperFilterTestData
   */
  public function testFilteredTextWrapper(string $prefix, string $suffix, string $content, string $expectedResult) {
    $this->wrapperFilter->setConfiguration([
      'settings' => [
        'prefix' => $prefix,
        'suffix' => $suffix,
      ],
    ]);

    $processed = (string) $this->wrapperFilter->process($content, Language::LANGCODE_NOT_SPECIFIED);
    $this->assertSame($processed, $expectedResult);
  }

  /**
   * Test data provider.
   */
  public function provideWrapperFilterTestData() {
    yield 'Basic wrapping div with class' => [
      '<div class="wysiwyg">',
      '</div>',
      'test',
      '<div class="wysiwyg">test</div>',
    ];

    yield 'Wrapping web component' => [
      '<my-component>',
      '</my-component>',
      'test',
      '<my-component>test</my-component>',
    ];

    yield 'Just prefix' => [
      '<em>Once upon a time...</em>',
      '',
      'test',
      '<em>Once upon a time...</em>test',
    ];
  }

}
