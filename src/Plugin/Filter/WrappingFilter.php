<?php

declare(strict_types=1);

namespace Drupal\filtered_text_wrapper\Plugin\Filter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides the Wrapping filter.
 *
 * @Filter(
 *   id = "wrapper",
 *   title = @Translation("Wrapper"),
 *   description = @Translation("Wrap the filtered text."),
 *   settings = {
 *     "prefix" = "<div class=""wysiwyg"">",
 *     "suffix" = "</div>",
 *   },
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class WrappingFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $text = $this->settings['prefix'] . $text . $this->settings['suffix'];
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return [
      'prefix' => [
        '#type' => 'textfield',
        '#title' => $this->t('Prefix'),
        '#description' => $this->t('Markup to add to the beginning of the filtered text, like an opening tag.'),
        '#default_value' => $this->settings['prefix'],
      ],
      'suffix' => [
        '#type' => 'textfield',
        '#title' => $this->t('Suffix'),
        '#description' => $this->t('Markup to add to the end of the filtered text, like a closing tag.'),
        '#default_value' => $this->settings['suffix'],
      ],
    ];
  }

}
